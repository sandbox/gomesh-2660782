==================
Theme - Incroyable
==================

Installation
===============
1. Extract the zip/tar.gz file and copy the folder into " Drupal-root/themes/ "
2. Goto <your_site>/admin/appearance and install and set default the Bootstrap incroyable theme.

Menu icon configuration
==========================
In the incroyable_preprocess_menu() function found in incroyable.theme, icon is already added for default menu.

To do the same for your custom menu title : 
else if ($menu_link['title'] == 'Contact') {
  $menu_link['title'] =  "<img src='" . THEME_PATH . '/flat-icons/mail.png' . "'  height='60' weight='60' /> " . $menu_link['title'];
}

add this php line in the if condition with your new menu title and icon image.
