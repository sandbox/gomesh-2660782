/**
 * @file
 * Custom JQuery scripts in this file.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.customIncroyable = {
    attach: function (context, settings) {
      $('body').addClass('js');

      $(document).ready(function () {

        $(window).scroll(function () {
          var scroll = $(window).scrollTop();
          if (scroll >= 100) {
            $('body').addClass('scrolling');
          }
          else {
            $('body').removeClass('scrolling');
          }
        });

        var device_width = $(window).width();

        // Provide for classes based on various widths.
        if (device_width <= 480) {
          $('html').addClass('mobile').removeClass('desktop').removeClass('tablet');
        }
        if (device_width > 480 && device_width < 768) {
          $('html').addClass('tablet').removeClass('mobile').removeClass('desktop');
        }
        if (device_width >= 768) {
          $('html').addClass('desktop').removeClass('mobile').removeClass('tablet');
        }
        $('.js #mobile-nav').click(function (e) {
          $('body').toggleClass('active');
          e.preventDefault();
        });

        $('.js .submenu-button').click(function (e) {
          $(this).parent().toggleClass('open');
        });
      });

      $(window).resize(function () {
        var device_width = $(window).width();
        if (device_width <= 480) {
          $('html').addClass('mobile').removeClass('desktop').removeClass('tablet');
        }
        if (device_width > 480 && device_width < 768) {
          $('html').addClass('tablet').removeClass('mobile').removeClass('desktop');
        }
        if (device_width >= 768) {
          $('html').addClass('desktop').removeClass('mobile').removeClass('tablet');
        }
      });
    }
  };

})(jQuery);
